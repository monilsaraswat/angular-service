myApp.controller("getCtrl", [
    '$scope',
    'fetchData',
    function ($scope, fetchData) {

       $scope.getData =  function(){
            fetchData.get().then(function (response) {
                $scope.listGet = response.data;
              }, function (error) {
                console.log(error);
            });
        };
    }
]);