myApp.controller("postCtrl", [
    '$scope',
    'fetchData',
    function ($scope, fetchData) {
        $scope.postData = function () {

            var params = {
                "name":$scope.postName,
                "job": $scope.postJob 
            };

            fetchData.post(params).then(function (response) {
                $scope.listPost = response;
            }, function (error) {
                console.log(error);
            });
        }
    }
]);