myApp.controller("putCtrl", [
    '$scope',
    'fetchData',
    function ($scope, fetchData) {
        
        $scope.putData = function () {
            var params = {
                name: $scope.putName,
                job: $scope.putJob
            };
            fetchData.put(params).then(function (response) {
                $scope.listPut = response;
            }, function (error) {
                console.log(error);
            })
        }
    }
]);