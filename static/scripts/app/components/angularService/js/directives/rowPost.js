myApp.directive("rowPost", [
    function() {
        return {
            restrict: 'EA',
            templateUrl : Url.resolveTemplateUrl('angularService/partials/rowPost.html'),
            controller:'postCtrl'
        }
    }
]);