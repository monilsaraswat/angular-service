myApp.factory('fetchData', [
    '$resource',
    '$q',
    function ($resource, $q) {

        var fetch = $resource(Url.resolveUrl("api/users?page=2"), {}, {
            get: {
                method: 'GET',
                isArray: false
            }
        });

        var update = $resource(Url.resolveUrl('api/users'), {}, {
            post: {
                method: 'POST',
                isArray: false
            }
        });

        var putResource = $resource(Url.resolveUrl('api/users/2'), {}, {
            put: {
                method: 'PUT',
                isArray: false
            }
        });

        return {
            get: function () {
                var defferd = $q.defer();
                fetch.get({}, function (response) {
                    defferd.resolve(response);
                    // return data;  
                }, function (error) {
                    defferd.reject(error);
                    // alert("Not Found");
                });
                return defferd.promise;
            },
            post: function (params) {
                var defferd = $q.defer();
                update.post({}, params, function (response) {
                        defferd.resolve(response);
                    },
                    function (error) {
                        defferd.reject(error);
                    });
                return defferd.promise;
            },
            put: function (params) {
                var deferd = $q.defer();
                putResource.put({updatedAt: 'Updated By monil'}, params, function (response) {
                        deferd.resolve(response);
                    },
                    function (error) {
                        deferd.reject(error);
                    });
                return deferd.promise;
            }
        }
    }
]);